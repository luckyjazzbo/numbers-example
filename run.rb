#!/usr/bin/env ruby

(1..100).each do |number|
  case 0
  when number % 15
    puts "FizzBuzz"
  when number % 5
    puts "Buzz"
  when number % 3
    puts "Fizz"
  else
    puts number
  end
end
